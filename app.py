import requests
import json

FIRST_NAME = 'Nick'
LAST_NAME = 'Gover'


def print_some_stuff():
    print('My name is ' + FIRST_NAME + ' ' + LAST_NAME)


def get_and_parse_json():
    print('Hey, it is working')
    r = requests.get('https://jsonplaceholder.typicode.com/todos/1')
    responseBody = r.text

    jsonString = json.loads(responseBody)
    title = jsonString["title"]

    print(title)

get_and_parse_json()


